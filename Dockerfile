FROM node:8
COPY package.json /opt/dockerized-ionic/
WORKDIR /opt/dockerized-ionic
RUN npm install -g npm@latest
RUN npm install -g cordova && npm install -g ionic@3.20.0
RUN npm install && npm cache verify
COPY . /opt/dockerized-ionic
