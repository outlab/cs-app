import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { IonicStorageModule } from '@ionic/storage';
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
import { HttpModule } from "@angular/http";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { CallNumber } from '@ionic-native/call-number';
import { Firebase } from '@ionic-native/firebase';

import { MyApp } from "./app.component";
import { HomePage } from "../pages/home/home";
import { LoginPage } from "../pages/login/login";
import { LandingPage } from "../pages/landing/landing";
import { CadastroPage } from "../pages/cadastro/cadastro";
import { EsqueceuSenhaPage } from "../pages/esqueceu-senha/esqueceu-senha";
import { PrefornecedorPage } from "../pages/prefornecedor/prefornecedor";
import { ResultadoBuscaPage } from "../pages/resultado-busca/resultado-busca";
import { ResultadoBuscaPerfilPage } from "../pages/resultado-busca-perfil/resultado-busca-perfil";
import { OrcamentosSolicitadosPage } from "../pages/orcamentos-solicitados/orcamentos-solicitados";
import { TermosCondicoesPage } from "../pages/termos-condicoes/termos-condicoes";
import { MeuPerfilPage } from "../pages/meu-perfil/meu-perfil";
import { FaqPage } from "../pages/faq/faq";
import { PoliticaPrivacidadePage } from "../pages/politica-privacidade/politica-privacidade";
import { FaleConoscoPage } from "../pages/fale-conosco/fale-conosco";

import { HomeColaboradorPage } from "../pages/home-colaborador/home-colaborador";
import { OrcamentosRecebidosPage } from "../pages/orcamentos-recebidos/orcamentos-recebidos";
import { FeedbacksRecebidosPage } from "../pages/feedbacks-recebidos/feedbacks-recebidos";

import { MenuComponent } from "../components/menu/menu";

import { AuthProvider } from "../providers/auth/auth";
import { ParseProvider } from "../providers/parse/parse";
import { ParamsService } from '../providers/params/params.service';
import { CvalidatorService } from '../providers/cvalidator/cvalidator.service';
import { SearchService } from '../providers/search/search.service';

@NgModule({
  declarations: [
    MyApp,
    MenuComponent,
    HomePage,
    LoginPage,
    LandingPage,
    CadastroPage,
    EsqueceuSenhaPage,
    PrefornecedorPage,
    ResultadoBuscaPage,
    ResultadoBuscaPerfilPage,
    OrcamentosSolicitadosPage,
    FaleConoscoPage,
    FaqPage,
    PoliticaPrivacidadePage,
    MeuPerfilPage,
    TermosCondicoesPage,
    HomeColaboradorPage,
    OrcamentosRecebidosPage,
    FeedbacksRecebidosPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      mode: "md"
    }),
    IonicStorageModule.forRoot({
      name: '__colaborasol',
      storeName: 'searchparams'
    }),
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MenuComponent,
    HomePage,
    LoginPage,
    LandingPage,
    CadastroPage,
    EsqueceuSenhaPage,
    PrefornecedorPage,
    ResultadoBuscaPage,
    ResultadoBuscaPerfilPage,
    OrcamentosSolicitadosPage,
    FaleConoscoPage,
    FaqPage,
    PoliticaPrivacidadePage,
    MeuPerfilPage,
    TermosCondicoesPage,
    HomeColaboradorPage,
    OrcamentosRecebidosPage,
    FeedbacksRecebidosPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ParseProvider,
    AuthProvider,
    ParamsService,
    CvalidatorService,
    SearchService,
    ScreenOrientation,
    CallNumber,
    Firebase
  ]
})
export class AppModule {}
