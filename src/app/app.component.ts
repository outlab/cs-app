import { Component } from '@angular/core';
import { ViewChild } from '@angular/core';

import { LoadingController } from "ionic-angular";
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

import { LandingPage } from '../pages/landing/landing';
import { LoginPage } from "../pages/login/login";
import { HomePage } from "../pages/home/home";
import { TermosCondicoesPage } from "../pages/termos-condicoes/termos-condicoes";
import { FaqPage } from "../pages/faq/faq";
import { FaleConoscoPage } from "../pages/fale-conosco/fale-conosco";
import { MeuPerfilPage } from "../pages/meu-perfil/meu-perfil";
import { OrcamentosSolicitadosPage } from "../pages/orcamentos-solicitados/orcamentos-solicitados";
import { PoliticaPrivacidadePage } from "../pages/politica-privacidade/politica-privacidade"
import { HomeColaboradorPage } from "../pages/home-colaborador/home-colaborador";
import { OrcamentosRecebidosPage } from "../pages/orcamentos-recebidos/orcamentos-recebidos";
import { FeedbacksRecebidosPage } from "../pages/feedbacks-recebidos/feedbacks-recebidos";

// Providers
import { AuthProvider } from "../providers/auth/auth"

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = LandingPage;

  @ViewChild(Nav) navCtrl: Nav;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private authPvdr: AuthProvider,
    private loadCtrl: LoadingController,
    private screenOrientation: ScreenOrientation) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    console.log(this.screenOrientation.type);

    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);

  }

  // this.firebase.getToken(){
  //   .then(token => console.log(`The token is ${token}`)) // save the token server-side and use it to push notifications to this device
  //   .catch(error => console.error('Error getting token', error));
  // }

  // this.firebase.onTokenRefresh(){
  //   .subscribe((token: string) => console.log(`Got a new token ${token}`));
  // }

  openHome() {
    this.navCtrl.push(HomePage);
  }

  openTermos() {
    this.navCtrl.push(TermosCondicoesPage);
  }

  openPolitica() {
    this.navCtrl.push(PoliticaPrivacidadePage);
  }

  openFaq() {
    this.navCtrl.push(FaqPage);
  }

  openFaleconosco() {
    this.navCtrl.push(FaleConoscoPage);
  }

  openMeuperfil() {
    this.navCtrl.push(MeuPerfilPage);
  }

  openOrcamentosSolicitados() {
    this.navCtrl.push(OrcamentosSolicitadosPage);
  }

  openHomeColaborador() {
    this.navCtrl.push(HomeColaboradorPage);
  }

  openOrcamentosRecebidos() {
    this.navCtrl.push(OrcamentosRecebidosPage);
  }

  openFeedbacksRecebidos() {
    this.navCtrl.push(FeedbacksRecebidosPage);
  }

  public doSignout() {
    let loader = this.loadCtrl.create({
      content: "Saindo..."
    });
    loader.present();

    this.authPvdr.signout().subscribe(
      success => {
        this.navCtrl.setRoot(LoginPage);
        loader.dismissAll();
        console.log("Logout feito com sucesso"); // TODO delete after tests
      },
      error => {
        alert("Não foi possível fazer logout");
        console.log("Não foi possível fazer logout"); // TODO delete after tests
        loader.dismissAll();
      }
    );
  }
}
