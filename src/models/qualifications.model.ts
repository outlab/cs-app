export class Qualifications {
  constructor(
    public id: string,
    public qualId: string,
    public name: string,
  ) { }
}
