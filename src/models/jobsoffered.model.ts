export class JobsOffered {
  constructor(
    public id: string,
    public jobId: string,
    public name: string
  ) { }
}
