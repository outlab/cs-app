export class SearchParams {

  constructor(
    public objId: string,
    public className: string,
    public name: string,
    public checked?: boolean,
    public found?: boolean
  ) { }

}
