import { Qualifications } from './qualifications.model';
import { JobsOffered } from './jobsoffered.model';

export class Supplier {
  constructor(
    public id: string,
    public uid: string,
    public cnpj?: string,
    public companyType?: string,
    public finishedJobs?: number,
    public supplierField?: string,
    public xp?: number,
    public uname?: string,
    public uemail?: string,
    public uphone?: string,
    public ucityId?: string,
    public city?: string,
    public qualifications?: Qualifications[],
    public jobsOffered?: JobsOffered[]
  ) {
    qualifications = [];
    jobsOffered = [];
  }
}
