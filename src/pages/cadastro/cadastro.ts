import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NavController, NavParams, LoadingController, AlertController } from "ionic-angular";

// Providers
import { AuthProvider } from "../../providers/auth/auth";
import { ParamsService } from './../../providers/params/params.service';

// Constants
import { HomePage } from "../home/home";
import { ENV } from "../../app/app.constant";
import { CvalidatorService } from '../../providers/cvalidator/cvalidator.service';

@Component({
  selector: "page-cadastro",
  templateUrl: "cadastro.html"
})
export class CadastroPage {
  signupForm: FormGroup;

  constructor(
    private authPvdr: AuthProvider,
    public formBuilder: FormBuilder,
    public navCtrl: NavController,
    private loadCtrl: LoadingController,
    public navParams: NavParams,
    public sparamsService: ParamsService,
    public alertCtrl: AlertController
  ) { }

  ngOnInit() {
    let passwordRegex = ENV.passwordRegex;
    let phoneRegex = ENV.phoneRegex;

    this.signupForm = this.formBuilder.group({
      name: ["", Validators.compose([Validators.required, Validators.minLength(3)])],
      email: ["", Validators.compose([Validators.required, Validators.email])],
      phone: ["", Validators.compose([Validators.required, Validators.pattern(phoneRegex), Validators.maxLength(11)])],
      city: ["", Validators.required],
      password: ["", Validators.compose([Validators.required, Validators.pattern(passwordRegex)])],
      confirmPassword: ["", Validators.required]
    }, {
        validator: CvalidatorService.MatchPassword
      });
  }

  get name() { return this.signupForm.get('name'); }
  get email() { return this.signupForm.get('email'); }
  get phone() { return this.signupForm.get('phone'); }
  get city() { return this.signupForm.get('city'); }
  get password() { return this.signupForm.get('password'); }
  get confirmPassword() { return this.signupForm.get('confirmPassword'); }

  onSubmit(): void {
    let formUser = this.signupForm.value;

    let password = formUser.password;
    delete formUser.password;
    delete formUser.confirmPassword;

    let loader = this.loadCtrl.create({
      content: "Aguarde..."
    });
    loader.present();

    this.authPvdr.signUp(formUser, password).subscribe(
      success => {
        this.sparamsService.runAllCloudCodes()
          .then(() => {
            loader.dismissAll();
          }).then(() => {
            this.navCtrl.setRoot(HomePage);
          }).catch((err) => {
            console.log('erro ', err);
          });
      },
      error => {
        console.log("ERRO!" + error);
        let prompt = this.alertCtrl.create({
          title: 'Algo saiu errado 😥',
          message: "Você precisa preencher corretamente os campos para fazer o seu cadastro.",
          buttons: [
            {
              text: 'Voltar',
              handler: data => {
                console.log('Voltar clicked');
              }
            }
          ]
        });
        prompt.present();
        loader.dismissAll();
      }
    );
  }

  ionViewDidLoad() {
  }
}
