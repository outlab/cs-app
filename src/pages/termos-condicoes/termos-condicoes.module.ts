import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TermosCondicoesPage } from './termos-condicoes';

import { MenuComponent }from '../../components/menu/menu';

@NgModule({
  declarations: [
    TermosCondicoesPage,
    MenuComponent
  ],
  imports: [
    IonicPageModule.forChild(TermosCondicoesPage),
  ],
})
export class TermosCondicoesPageModule {}
