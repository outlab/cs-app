import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrcamentosSolicitadosPage } from './orcamentos-solicitados';

@NgModule({
  declarations: [
    OrcamentosSolicitadosPage,
  ],
  imports: [
    IonicPageModule.forChild(OrcamentosSolicitadosPage),
  ],
})
export class OrcamentosSolicitadosPageModule {}
