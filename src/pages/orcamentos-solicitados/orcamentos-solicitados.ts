import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-orcamentos-solicitados',
  templateUrl: 'orcamentos-solicitados.html',
})
export class OrcamentosSolicitadosPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
  }

  enviarFeedback() {
    let prompt = this.alertCtrl.create({
      title: 'Enviar Feedback',
      message: "Digite aqui o seu feedback para o nosso colaborador:",
      inputs: [
        {
          name: 'title',
          placeholder: 'Escreva sua mensagem',
          type: 'text',
          value: 'Valor de teste para o input de feedback do colaborador'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Enviar Feedback',
          handler: data => {
            console.log('Saved clicked');
          }
        }
      ]
    });
    prompt.present();
  }

}
