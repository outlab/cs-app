import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultadoBuscaPerfilPage } from './resultado-busca-perfil';

import { MenuComponent }from '../../components/menu/menu';

@NgModule({
  declarations: [
    ResultadoBuscaPerfilPage,
    MenuComponent
  ],
  imports: [
    IonicPageModule.forChild(ResultadoBuscaPerfilPage),
  ],
})
export class ResultadoBuscaPerfilPageModule {}
