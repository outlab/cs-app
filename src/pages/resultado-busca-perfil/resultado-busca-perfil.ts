import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';

import { Supplier } from './../../models/supplier.model';

@IonicPage()
@Component({
  selector: 'page-resultado-busca-perfil',
  templateUrl: 'resultado-busca-perfil.html',
})
export class ResultadoBuscaPerfilPage {
  supplier: Supplier;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private callNumber: CallNumber) {
    this.supplier = this.navParams.data;
  }

  ionViewDidLoad() {
  }

  callSupplier(phone: string) {
    this.callNumber.callNumber(phone, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

  solicitaOrcamento() {
    let prompt = this.alertCtrl.create({
      title: 'Solicitar Orçamento',
      message: "Digite aqui a sua mensagem para o nosso colaborador:",
      inputs: [
        {
          name: 'title',
          placeholder: 'Descreva como podemos te ajudar',
          type: 'text',
          value: 'Valor de teste para o input de orçamento no perfil do colaborador'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Solicitar Orçamento',
          handler: data => {
            console.log('Saved clicked');
          }
        }
      ]
    });
    prompt.present();
  }

}
