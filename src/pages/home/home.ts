import { SearchService } from './../../providers/search/search.service';
import { Supplier } from './../../models/supplier.model';
import { ParamsService } from './../../providers/params/params.service';
import { Component } from "@angular/core";
import { NavController, AlertController, LoadingController } from "ionic-angular";

import { ResultadoBuscaPage } from "../resultado-busca/resultado-busca";

import { SearchParams } from './../../models/searchparams.model';

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  pushResultadoBuscaPage: any;

  searchParams: SearchParams[] = [];
  selectedCity: SearchParams;
  selectedJobType: SearchParams;
  selectedQualifications: SearchParams[] = [];
  suppliersFinalList: Supplier[] = [];

  constructor(
    public navCtrl: NavController,
    public sparamsService: ParamsService,
    public alertCtrl: AlertController,
    private loadCtrl: LoadingController,
    public searchSvc: SearchService
  ) {
    this.pushResultadoBuscaPage = ResultadoBuscaPage;
  }

  ionViewDidLoad() {
    this.sparamsService.getAll()
      .then((param: SearchParams[]) => {
        this.searchParams = param;
      });
  }

  search() {
    let loader = this.loadCtrl.create({
      content: "Buscando em nossos servidores..."
    });
    let prompt = this.alertCtrl.create({
      title: 'Ops! Parece que você esqueceu algo. 🤔',
      message: "Escolha o tipo de serviço e as qualificações do profissional para fazer a busca.",
      buttons: [
        {
          text: 'Voltar',
          handler: data => {
          }
        }
      ]
    });

    if (this.selectedCity != undefined && this.selectedJobType != undefined) {
      loader.present();
      this.sendSearchParams()
        .then((paramsId) => {
          this.searchSvc.runSearchInCloud(paramsId)
            .then((result) => {
              this.sendSeach(result)
                .then(() => loader.dismissAll());
            }).catch((err) => {
              console.log('ERRO do newSearch', err);
              prompt.present();
              loader.dismiss();
            });
        }).catch((err) => {
          console.log('ERRO do sendSearchParams', err);
          prompt.present();
          loader.dismiss();
        });
    } else {
      prompt.present();
      loader.dismiss();
    }
  }

  sendSeach(suppliersSet: any): Promise<any> {
    return this.searchSvc.searchResultsModeler(suppliersSet)
      .then((suppliers) => this.navCtrl.push(this.pushResultadoBuscaPage, suppliers));
  }

  selectItemQualification(data) {
    if (data.checked == true) {
      this.selectedQualifications.push(data);
    } else {
      let newArray = this.selectedQualifications.filter((el) => {
        return el.objId !== data.objId;
      });
      this.selectedQualifications = newArray;
    }
  }

  sendSearchParams(): Promise<any> {
    let p = new Promise<any>((res, rej) => {
      let quals = [];
      let paramsID = {
        qualIds: [],
        cityId: "",
        jobId: "",
      }
      for (let i = 0; i < this.selectedQualifications.length; i++) {
        console.log('qual i ', this.selectedQualifications[i].objId);
        quals.push(this.selectedQualifications[i].objId);
      }
      if (quals.length === this.selectedQualifications.length) {
        console.log('s quals ', quals);
        console.log('s city ', this.selectedCity);
        console.log('s job ', this.selectedJobType);
        paramsID.cityId = this.selectedCity.objId;
        paramsID.jobId = this.selectedJobType.objId;
        paramsID.qualIds = quals;
        console.log("params ID", paramsID);
        res(paramsID);
      } else {
        console.log('s quals ', quals);
        console.log('s city ', this.selectedCity);
        console.log('s job ', this.selectedJobType);
        rej('Erro no for');
      }
    });
    return p;
  }

}
