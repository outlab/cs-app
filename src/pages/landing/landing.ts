import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';

import { LoginPage } from '../login/login'
import { HomePage } from '../home/home';

import { Parse } from 'parse';

// Providers
import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})

export class LandingPage {

  pushPage: any;
  pushPageHome: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private authPvdr: AuthProvider) {
    this.pushPage = LoginPage;
    this.pushPageHome = HomePage;
  }

  ionViewDidLoad() {
    let currentUser = Parse.User.current();
    if (currentUser) {
      this.goHome();
    } else {
      this.authPvdr.signout(); //TODO implementar ionic auth guard
    }
  }

  @ViewChild(Slides) slides: Slides;

  goToSlide() {
    this.slides.slideTo(2, 500);
  }

  goHome() {
    this.navCtrl.setRoot(HomePage);
  }

}
