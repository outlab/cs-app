import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ResultadoBuscaPerfilPage } from '../resultado-busca-perfil/resultado-busca-perfil';
import 'rxjs/add/operator/map'

import { AuthProvider } from '../../providers/auth/auth';
import { SearchService } from '../../providers/search/search.service';

import { Supplier } from './../../models/supplier.model';

@IonicPage()
@Component({
  selector: 'page-resultado-busca',
  templateUrl: 'resultado-busca.html'
})
export class ResultadoBuscaPage {
  pushResultadoBuscaPerfilPage: any;
  resultSearch: any[];

  suppliersFinalList: Supplier[] = this.navParams.data;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private authPvdr: AuthProvider,
    public searchPvdr: SearchService,
  ) {
    this.pushResultadoBuscaPerfilPage = ResultadoBuscaPerfilPage;
    // this.suppliersFinalList = this.searchPvdr.suppliersFinalList; //mudar p/ didload
  }

  ionViewDidLoad() {
    // implementar essa promise no searchPvdr?? =
    // this.searchPvdr.getAll()
    //   .then(suppliers: Supplier[]) => {
    //   this.suppliersFinalList = suppliers;
    // });
  }

  ionViewCanEnter(): boolean {
    return this.authPvdr.authenticated();
  }

  getSupplierDetails(supplier: Supplier) {
    this.navCtrl.push(this.pushResultadoBuscaPerfilPage, supplier);
  }

}
