import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultadoBuscaPage } from './resultado-busca';

import { MenuComponent }from '../../components/menu/menu';

@NgModule({
  declarations: [
    ResultadoBuscaPage,
    MenuComponent
  ],
  imports: [
    IonicPageModule.forChild(ResultadoBuscaPage),
  ],
})
export class ResultadoBuscaPageModule {}
