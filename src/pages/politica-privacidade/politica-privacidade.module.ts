import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PoliticaPrivacidadePage } from './politica-privacidade';

import { MenuComponent }from '../../components/menu/menu';

@NgModule({
  declarations: [
    PoliticaPrivacidadePage,
    MenuComponent
  ],
  imports: [
    IonicPageModule.forChild(PoliticaPrivacidadePage),
  ],
})
export class PoliticaPrivacidadePageModule {}
