import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeedbacksRecebidosPage } from './feedbacks-recebidos';

@NgModule({
  declarations: [
    FeedbacksRecebidosPage,
  ],
  imports: [
    IonicPageModule.forChild(FeedbacksRecebidosPage),
  ],
})
export class FeedbacksRecebidosPageModule {}
