import { Component } from "@angular/core";
import { NavController, NavParams, LoadingController, AlertController } from "ionic-angular";

import { CadastroPage } from '../cadastro/cadastro';
import { EsqueceuSenhaPage } from '../esqueceu-senha/esqueceu-senha';
import { PrefornecedorPage } from '../prefornecedor/prefornecedor';
import { HomePage } from '../home/home';

// Providers
import { AuthProvider } from '../../providers/auth/auth';
import { ParamsService } from './../../providers/params/params.service';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  pushPagePrefornecedor: any;
  pushPageSenha: any;
  pushPageCadastro: any;
  pushPageHome: any;
  password: string = '';
  username: string = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private loadCtrl: LoadingController,
    private authPvdr: AuthProvider,
    public sparamsService: ParamsService,
    public alertCtrl: AlertController
  ) {
    this.pushPageSenha = EsqueceuSenhaPage;
    this.pushPageCadastro = CadastroPage;
    this.pushPagePrefornecedor = PrefornecedorPage;
    this.pushPageHome = HomePage;
  }

  goHome() {
    this.navCtrl.setRoot(HomePage);
  }

  ionViewDidLoad() {
    this.authPvdr.signout(); //TODO implementar ionic auth guard
  }

  public skipSignin() {
    this.username = 'teste@test.com';
    this.password = 'Qwert6y';

    this.doSignin();
  }

  public doSignin() {
    let loader = this.loadCtrl.create({
      content: "Aguarde..."
    });
    loader.present();

    this.authPvdr.signin(this.username, this.password).subscribe(
      success => {
        this.sparamsService.runAllCloudCodes()
          .then(() => {
            loader.dismissAll();
          }).then(() => {
            this.goHome();
          }).catch((err) => {
            console.log('erro ', err);
          });
      },
      error => {
        let prompt = this.alertCtrl.create({
          title: 'Algo saiu errado 😥',
          message: "Você precisa preencher corretamente os campos para acessar o ColaboraSol.",
          buttons: [
            {
              text: 'Voltar',
              handler: data => {
                console.log('Voltar clicked');
              }
            }
          ]
        });
        prompt.present();
        loader.dismissAll();
      }
    );
  }
}
