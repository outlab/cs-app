import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-orcamentos-recebidos',
  templateUrl: 'orcamentos-recebidos.html',
})
export class OrcamentosRecebidosPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
  }

  respoderOrcamento() {
    let prompt = this.alertCtrl.create({
      title: 'Rsponder Orçamento',
      message: "Digite aqui a sua resposta para o orçamento solicitado:",
      inputs: [
        {
          name: 'title',
          placeholder: 'Escreva sua mensagem',
          type: 'text',
          value: 'Valor de teste para o input de resposta do colaborador'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Enviar Resposta',
          handler: data => {
            console.log('Saved clicked');
          }
        }
      ]
    });
    prompt.present();
  }

}
