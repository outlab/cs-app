import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrcamentosRecebidosPage } from './orcamentos-recebidos';

import { MenuComponent }from '../../components/menu/menu';

@NgModule({
  declarations: [
    OrcamentosRecebidosPage,
    MenuComponent
  ],
  imports: [
    IonicPageModule.forChild(OrcamentosRecebidosPage),
  ],
})
export class OrcamentosRecebidosPageModule {}
