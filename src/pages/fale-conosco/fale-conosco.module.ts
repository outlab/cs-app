import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FaleConoscoPage } from './fale-conosco';

import { MenuComponent }from '../../components/menu/menu';

@NgModule({
  declarations: [
    FaleConoscoPage,
    MenuComponent
  ],
  imports: [
    IonicPageModule.forChild(FaleConoscoPage),
  ],
})
export class FaleConoscoPageModule {}
