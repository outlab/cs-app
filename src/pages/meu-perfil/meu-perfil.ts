import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { Parse } from 'parse';

// Providers
import { AuthProvider } from "../../providers/auth/auth";

// Constants
import { ENV } from "../../app/app.constant";
// import { CvalidatorService } from '../../providers/cvalidator/cvalidator.service';

@IonicPage()
@Component({
  selector: 'page-meu-perfil',
  templateUrl: 'meu-perfil.html',
})
export class MeuPerfilPage {
  updateForm: FormGroup;
  currentUser = Parse.User.current();
  user = this.currentUser.toJSON();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private authPvdr: AuthProvider,
    public formBuilder: FormBuilder,
    private loadCtrl: LoadingController,
    public alertCtrl: AlertController
  ) {

    let phoneRegex = ENV.phoneRegex;

    this.updateForm = this.formBuilder.group({
      name: [this.user.name, Validators.compose([Validators.required, Validators.minLength(3)])],
      email: [this.user.email, Validators.compose([Validators.required, Validators.email])],
      phone: [this.user.phone, Validators.compose([Validators.required, Validators.pattern(phoneRegex), Validators.maxLength(11)])],
      city: [this.user.state, Validators.required]
    });
  }

  ionViewDidLoad() {
  }

  pop() {
    this.navCtrl.pop();
  }

  get name() { return this.updateForm.get('name'); }
  get email() { return this.updateForm.get('email'); }
  get phone() { return this.updateForm.get('phone'); }
  get city() { return this.updateForm.get('city'); }

  public onSubmit() {
    let formUser = this.updateForm.value;

    let loader = this.loadCtrl.create({
      content: "Aguarde..."
    });
    loader.present();

    this.authPvdr.updateUser(formUser).subscribe(
      success => {
        let prompt = this.alertCtrl.create({
          title: 'Muito bem! 😁',
          message: "Seu cadastro foi atualizado com as novas informações.",
          buttons: [
            {
              text: 'Ok',
              handler: data => {
                console.log('ok clicked');
              }
            }
          ]
        });
        prompt.present();
        this.pop();
        loader.dismissAll();
      },
      error => {
        let prompt = this.alertCtrl.create({
          title: 'Algo saiu errado 😥',
          message: "Você precisa preencher corretamente os campos para atualizar seus dados",
          buttons: [
            {
              text: 'Voltar',
              handler: data => {
                console.log('Voltar clicked');
              }
            }
          ]
        });
        prompt.present();
        loader.dismissAll();
      }
    );
  }

}
