import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MeuPerfilPage } from './meu-perfil';

import { MenuComponent }from '../../components/menu/menu';

@NgModule({
  declarations: [
    MeuPerfilPage,
    MenuComponent
  ],
  imports: [
    IonicPageModule.forChild(MeuPerfilPage),
  ],
})
export class MeuPerfilPageModule {}
