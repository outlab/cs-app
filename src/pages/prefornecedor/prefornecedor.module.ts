import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrefornecedorPage } from './prefornecedor';

@NgModule({
  declarations: [
    PrefornecedorPage,
  ],
  imports: [
    IonicPageModule.forChild(PrefornecedorPage),
  ],
})
export class PrefornecedorPageModule {}
