import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Parse } from 'parse';
import { ParseProvider } from '../../providers/parse/parse';

/**
 * Generated class for the PrefornecedorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-prefornecedor',
  templateUrl: 'prefornecedor.html',
})
export class PrefornecedorPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public parsePvdr: ParseProvider) {
  }

  ionViewDidLoad() {
    this.printLegalTexts();
  }

  public getLegalTexts(a) {
    Parse.Cloud.run("getLegalTexts", { name: a })
    .then((result) => {
      if (result.length != 0) {
        var arr = JSON.stringify(result);
        arr = arr.replace("[{","{");
        arr = arr.replace("}]","}");
        var obj = JSON.parse(arr);
        document.getElementById("title").innerHTML = obj.title;
        document.getElementById("text").innerHTML = obj.text; // TODO carregar o texto final em HTML dentro do dashboard!!
      } else {
        console.log("text not found");
      }
    });
  }

  public printLegalTexts() {
    let name = "prefornecedor";
    this.getLegalTexts(name);
  }

}
