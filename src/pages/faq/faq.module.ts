import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FaqPage } from './faq';

import { MenuComponent }from '../../components/menu/menu';

@NgModule({
  declarations: [
    FaqPage,
    MenuComponent
  ],
  imports: [
    IonicPageModule.forChild(FaqPage),
  ],
})
export class FaqPageModule {}
