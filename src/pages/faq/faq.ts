import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import 'rxjs/add/operator/map'

import { Parse } from 'parse';
import { ParseProvider } from '../../providers/parse/parse';

@IonicPage()
@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html',
})
export class FaqPage {

  public allData:any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public parsePvdr: ParseProvider) {
  }

  // toggleSection(i) {
  //   this.information[i].open = !this.information[i].open;
  // }

  // toggleItem(i, j) {
  //   this.information[i].children[j].open = !this.information[i].children[j].open;
  // }

  selectedItem=0;

  ionViewDidLoad() {
    this.printFAQ();
  }

  public printFAQ() {
    Parse.Cloud.run("getFAQ", {})
    .then ((result) => {
      if (result.length != 0) {
        var items = JSON.stringify(result);
        this.allData = JSON.parse(items);
      } else {
        console.log("deu ruim");
      }
    })
  }



}
