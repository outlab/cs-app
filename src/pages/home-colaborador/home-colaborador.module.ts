import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeColaboradorPage } from './home-colaborador';

@NgModule({
  declarations: [
    HomeColaboradorPage,
  ],
  imports: [
    IonicPageModule.forChild(HomeColaboradorPage),
  ],
})
export class HomeColaboradorPageModule {}
