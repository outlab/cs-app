import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { OrcamentosRecebidosPage } from "../orcamentos-recebidos/orcamentos-recebidos";
import { FeedbacksRecebidosPage } from "../feedbacks-recebidos/feedbacks-recebidos";

@IonicPage()
@Component({
  selector: 'page-home-colaborador',
  templateUrl: 'home-colaborador.html',
})
export class HomeColaboradorPage {

	pushOrcamentosRecebidos: any;
	pushFeedbacksRecebidos: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.pushOrcamentosRecebidos = OrcamentosRecebidosPage;
  	this.pushFeedbacksRecebidos = FeedbacksRecebidosPage;
  }

  ionViewDidLoad() {
  }

}
