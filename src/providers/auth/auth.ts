import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";

import { User } from "../../models/user.model";

// Parse
import { Parse } from "parse";

// Constants
import { ENV } from "../../app/app.constant";

@Injectable()
export class AuthProvider {
  private parseAppId: string = ENV.parseAppId;
  private parseJSKey: string = ENV.parseJSKey;
  private parseMasterKey: string = ENV.parseMasterKey;
  private parseServerUrl: string = ENV.parseServerUrl;

  constructor(public http: HttpClient) {
    this.parseInitialize();
  }

  public signin(username: string, password: string): Observable<boolean> {
    return new Observable(observer => {
      Parse.User.logIn(username, password, {
        success: function(user) {
          observer.next(true);
          observer.complete();
        },
        error: function(user, error) {
          observer.error(error);
          observer.complete();
        }
      });
    });
  }

  public currentUser(): User {
    let u = Parse.User.current();
    if (u) {
      let user = new User(); // TODO WHY se declarar as coisas dentro do constructor do user isso aq nao funciona!
      user.id = u.id;
      user.name = u.get("name");
      user.email = u.get("email");
      return user;
    }
    return null;
  }

  public signUp(newuser: User, password: String): Observable<boolean> {
    return new Observable(observer => {
      if (this.currentUser() != null) {
        this.signout();
      }

      let user = new Parse.User();
      user.setUsername(newuser.email);
      user.set("email", newuser.email);
      user.set("password", password);
      user.set("name", newuser.name);
      user.set("phone", newuser.phone);
      user.set("state", newuser.city);
      user.set("userRole", "client");
      user.signUp(null, {
        success: user => {
          observer.next(true);
          observer.complete();
        },
        error: (user, error) => {
          observer.error(error);
          observer.complete();
        }
      });
    });
  }

  public signout(): Observable<boolean> {
    return new Observable(observer => {
      Parse.User.logOut().then(() => observer.next(true));
    });
  }

  public authenticated(): boolean {
    return this.currentUser() !== null;
  }

  public updateUser(user: User): Observable<boolean> {
    return new Observable(observer => {
      if (this.currentUser() === null) {
        observer.complete();
      }
      let u = Parse.User.current();
      u.setUsername(user.email);
      u.set("email", user.email);
      u.set("name", user.name);
      u.set("phone", user.phone);
      u.set("state", user.city);
      u.save(null, {
        success: user => {
          observer.next(true);
          observer.complete();
        },
        error: (user, error) => {
          observer.error(error);
          observer.complete();
        }
      });
    });
  }

  private parseInitialize() {
    Parse.initialize(this.parseAppId, this.parseJSKey, this.parseMasterKey);
    Parse.serverURL = this.parseServerUrl;
  }
}
