import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Parse } from 'parse';

import { Supplier } from './../../models/supplier.model';
import { Qualifications } from './../../models/qualifications.model'
import { JobsOffered } from './../../models/jobsoffered.model';

@Injectable()
export class SearchService {
  suppliersFinalList: Supplier[] = []

  constructor(http: HttpClient) {
  }

  runSearchInCloud(paramsId: any): Promise<any> {
    let promise = new Promise<any>((res, rej) => {
      Parse.Cloud.run('searchSuppliers', paramsId)
        .then((result) => {
          res(result);
        }).catch((err) => {
          rej(err);
        });
    })
    return promise;
  }

  searchResultsModeler(suppliersSet: any): Promise<Supplier[]> {
    let promise = new Promise<Supplier[]>((res, rej) => {
      this.suppliersFinalList = [];
      let sup: Supplier;

      suppliersSet.forEach(el => {
        let qual: Qualifications;
        let supQualList: Qualifications[] = [];

        el.qualObj.forEach(qualEl => {

          qual = new Qualifications(
            qualEl.id,
            qualEl.attributes.qualId,
            qualEl.attributes.name);

          supQualList.push(qual);
        });

        let job: JobsOffered;
        let supJobList: JobsOffered[] = [];

        el.jobsObj.forEach(jobEl => {

          job = new JobsOffered(
            jobEl.id,
            jobEl.attributes.jobId,
            jobEl.attributes.name);

          supJobList.push(job);
        });

        sup = new Supplier(
          el.id,
          el.userId.id,
          el.obj.attributes.cnpj,
          el.obj.attributes.companyType,
          el.obj.attributes.finishedJobs,
          el.obj.attributes.supplierField,
          el.obj.attributes.xp,
          el.userObj.attributes.name,
          el.userObj.attributes.username,
          el.userObj.attributes.phone,
          el.userObj.attributes.city.id,
          el.userObj.attributes.state,
          //TODO: tomar vergonha na cara e preencher isso do jeito correto!!! (precisa mudar todas as partes do app)
          supQualList,
          supJobList
        )
        this.suppliersFinalList.push(sup);
      });

      res(this.suppliersFinalList);
    });
    return promise;
  }

}
