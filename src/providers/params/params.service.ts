import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import { SearchParams } from './../../models/searchparams.model';

import { Parse } from 'parse';

@Injectable()
export class ParamsService {

  constructor(
    public storage: Storage
  ) { }

  getAll(): Promise<SearchParams[]> {

    return this.storage.ready()
      .then((localForage: LocalForage) => {

        let params: SearchParams[] = [];

        return this.storage.forEach((searchparam: SearchParams, key: string, iterationNumber: number) => {
          if (key.indexOf('params.') > -1) {
            params.push(searchparam);
          }
        }).then(() => params);
      });

    // }).catch(err => console.log('Erro ao abrir o storage: ', err));

  }

  get(id: string): Promise<SearchParams[]> {
    return this.storage.get(`params.${id}`)
      .catch(err => console.log('Erro ao pegar params: ', err));
  }

  create(searchparam: SearchParams): Promise<SearchParams[]> {
    return this.storage.set(`params.${searchparam.objId}`, searchparam);
  }

  delete(id: string): Promise<boolean> {
    return this.storage.remove(`params.${id}`)
      .then(() => true);
  }

  runAllCloudCodes(): Promise<SearchParams> {

    let data: SearchParams;

    return this.runCCForSearch(data, 'getLocation', 'Location', 'city')
      .then((data) => this.runCCForSearch(data, 'getJobType', 'JobType', 'name'))
      .then((data) => this.runCCForSearch(data, 'getQualifications', 'Qualifications', 'name'))
  }

  runCCForSearch(data: SearchParams, ccodeName: string, className: string, nameField: string): Promise<SearchParams> {

    return Parse.Cloud.run(ccodeName)
      .then((result) => {
        if (result.length != 0) {
          for (var i = 0; i < result.length; ++i) {
            let objId = result[i].id;
            let name = result[i].get(nameField);
            data = new SearchParams(objId, className, name);
            this.create(data);
          }
        } else {
          console.log('erro no if');
        }
      }).then(() => data);
  }

}
