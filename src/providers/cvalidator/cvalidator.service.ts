import { AbstractControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CvalidatorService {

  constructor(public http: HttpClient) {
  }

  // Validation for password and confirm password
  static MatchPassword(AC: AbstractControl) {
    const pwd = AC.get('password').value // to get value in input tag
    const confirmPwd = AC.get('confirmPassword').value // to get value in input tag
    if (pwd != confirmPwd) {
      // console.log('false');
      AC.get('confirmPassword').setErrors({ MatchPassword: true })
    } else {
      // console.log('true')
      AC.get('confirmPassword').setErrors(null);
    }
  }
}
