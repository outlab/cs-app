import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import "rxjs/add/operator/map";

// Parse
import { Parse } from 'parse';

// Constants
import { ENV } from '../../app/app.constant';

@Injectable()
export class ParseProvider {
  private parseAppId: string = ENV.parseAppId;
  private parseJSKey: string = ENV.parseJSKey;
  private parseMasterKey: string = ENV.parseMasterKey;
  private parseServerUrl: string = ENV.parseServerUrl;

  constructor(public http: HttpClient) {
    this.parseInitialize();
  }

  private parseInitialize() {
    Parse.initialize(this.parseAppId, this.parseJSKey, this.parseMasterKey);
    Parse.serverURL = this.parseServerUrl;
  }

  public userHasRole(user, roleName) {
    var query = new Parse.Query(Parse.Role);
    query.equalTo("name", roleName);
    query.equalTo("users", user);
    return query.find().then(function (roles) {
      return roles.length > 0;
    });
  }

}
